<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Christopher Morrow <morrow.christopherl@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\DirectDownloadLink\Tests\Unit\Controller;

use OCA\DirectDownloadLink\Controller\NoteApiController;

class NoteApiControllerTest extends NoteControllerTest {
	public function setUp(): void {
		parent::setUp();
		$this->controller = new NoteApiController($this->request, $this->service, $this->userId);
	}
}
