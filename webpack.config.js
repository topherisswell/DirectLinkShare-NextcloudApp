// SPDX-FileCopyrightText: Christopher Morrow <morrow.christopherl@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later
const webpackConfig = require('@nextcloud/webpack-vue-config')

module.exports = webpackConfig
