<!--
SPDX-FileCopyrightText: Christopher Morrow <morrow.christopherl@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->

# Direct Download Link
This is a Nextcloud App that adds a share link to the raw file for programmatic/direct download. This is an alternative to the default behavior which provides a link to a nextcloud page to download the shared file.

# Current Status
Work In Progress. Do not install unless you know what you're doing.

# Install
Place this app in **nextcloud/apps/**

## Building the app

The app can be built by using the provided Makefile by running:

```bash
    make
```

This requires the following things to be present:

- make
- which
- tar: for building the archive
- curl: used if phpunit and composer are not installed to fetch them from the web
- npm: for building and testing everything JS, only required if a package.json is placed inside the js/ folder


